========================
django-eurocom-project
========================

A project template for Django 1.5.

Creating your project
=====================

    $ django-admin.py startproject --template=https://bitbucket.org/eurocom/django-eurocom-project/get/master.zip --extension=py,rst,html project

Installation of Dependencies
============================

First, make sure you are using virtualenv (http://www.virtualenv.org).

Then, depending on where you are installing dependencies:

In development::

    $ pip install -r requirements/local.txt

For production::

    $ pip install -r requirements.txt

*note: We install production requirements this way because many Platforms as a Services expect a requirements.txt file in the root of projects.*
