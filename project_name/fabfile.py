import fabtools

from fabric.api import *
from fabric.colors import *
from fabric.contrib import files
from fabric.contrib.console import confirm
from fabric.operations import env, put
from fabtools import require


# Globals
env.warn_only = False
env.project = '{{ project_name }}'
env.repo = ''
env.code_dir = '~/sandbox/releases/'
env.activate_vars = {}


def info():
    setup_dir_envs()

    print yellow('-*-------------------------------------------------------------------------------------------*-')
    print yellow(' ___  ___ ___ _    _____   ____  __ ___ _  _ _____ ')
    print yellow('|   \| __| _ \ |  / _ \ \ / /  \/  | __| \| |_   _|')
    print yellow('| |) | _||  _/ |_| (_) \ V /| |\/| | _|| .` | | |  ')
    print yellow('|___/|___|_| |____\___/ |_| |_|  |_|___|_|\_| |_|  ')
    print ''
    print yellow('You are about to deploy {{ project_name }}. Please check that the information below is correct before')
    print yellow('continuing.')
    print ''
    print yellow('-*-------------------------------------------------------------------------------------------*-')
    print yellow('  Settings Being Used:')
    print yellow('-*-------------------------------------------------------------------------------------------*-')
    print yellow('  Hosts:           ') + green('%(hosts)s' % env)
    print yellow('  User:            ') + green('%(user)s' % env)
    print yellow('  Key File:        ') + green('%(key_filename)s' % env)
    print yellow('  Site Directory:  ') + green('%(sites_project_dir)s' % env)
    print yellow('  Code Repository: ') + green('%(repo)s' % env)
    print yellow('-*-------------------------------------------------------------------------------------------*-')

    if not confirm(red('Happy to continue?'), default=False):
        abort(red('Aborting at user request.'))


def done():
    print ''
    print yellow('-*-------------------------------------------------------------------------------------------*-')
    print yellow('  ___   ___  _  _ ___ ')
    print yellow(' |   \ / _ \| \| | __|')
    print yellow(' | |) | (_) | .` | _| ')
    print yellow(' |___/ \___/|_|\_|___|')
    print ''
    print yellow('-*-------------------------------------------------------------------------------------------*-')


def setup_dir_envs():
    env.sites_dir = '/home/%(user)s/sites/' % env
    env.sites_project_dir = '/home/%(user)s/sites/%(deployment_app_name)s' % env
    env.sites_project_var_dir = '/home/%(user)s/sites/%(deployment_app_name)s/var' % env
    env.sites_project_log_dir = '/home/%(user)s/sites/%(deployment_app_name)s/var/logs' % env

    env.releases_dir = '~/sandbox/releases' % env
    env.releases_project_dir = '~/sandbox/releases/%(deployment_app_name)s' % env

    env.virtualenv_dir = '/home/%(user)s/envs' % env
    env.virtualenv_project_dir = '/home/%(user)s/envs/%(deployment_app_name)s' % env

    env.supervisord_dir = '/etc/supervisord' % env
    env.supervisord_conf_dir = '/etc/supervisord/conf.d' % env


def dev():
    env.hosts = [
    ]
    env.roledefs = {
        'web': [
        ],
        'queue': [
        ],
        'db': [
        ]
    }
    env.key_filename = ''
    env.user = 'ubuntu'
    env.user_dir = '/home/%(user)s/' % env
    env.deployment_app_name = '{{ project_name }}dev'

    info()


def get_activate_vars():
    if not len(env.activate_vars):
        print green('Please set the following environment variables:')
        print

        done = False

        while not done:
            print green('Email server details:')
            email_host = prompt(yellow('EMAIL_HOST: '), default='smtp.gmail.com')
            email_port = prompt(yellow('EMAIL_PORT: '), default='587')
            email_host_user = prompt(yellow('EMAIL_HOST_USER: '), default='%(deployment_app_name)s@colonyhq.com' % env)
            email_host_password = prompt(yellow('EMAIL_HOST_PASSWORD: '))

            print
            print yellow('Database server details:')
            db_host = prompt(yellow('DB_HOST: '), default=env.roledefs['db'][0])
            db_port = prompt(yellow('DB_PORT: '), default='5432')
            db_name = prompt(yellow('DB_NAME: '), default='%(project)sdb' % env)
            db_user = prompt(yellow('DB_USER: '), default='%(project)sadmin' % env)
            db_password = prompt(yellow('DB_PASSWORD: '))

            print
            print green('Django app details:')
            secret_key = prompt(yellow('SECRET_KEY: '))

            if not len(email_host) or not len(email_port) or not len(email_host_user) or \
               not len(email_host_password) or not len(db_host) or not len(db_port) or not len(db_name) or \
               not len(db_user) or not len(db_password) or not len(secret_key):
                print red('You have not entered some values. All values must be entered!')
                done = False
            else:
                done = True

        env.activate_vars.update({
            'email_host': email_host,
            'email_port': email_port,
            'email_host_user': email_host_user,
            'email_host_password': email_host_password,
            'db_host': db_host,
            'db_port': db_port,
            'db_name': db_name,
            'db_user': db_user,
            'db_password': db_password,
            'secret_key': secret_key,
        })


@roles('web', 'queue')
def write_env_vars():
    print green('Writing the environment variables...')

    # Setting EMAIL_HOST environment variable.
    print yellow('Checking for EMAIL_HOST...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export EMAIL_HOST=', exact=True):
        print yellow('Updating EMAIL_HOST...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export EMAIL_HOST=[.*]",
                  after="export EMAIL_HOST=%s" % env.activate_vars['email_host'],
                  backup='')
    else:
        print yellow('Adding EMAIL_HOST...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export EMAIL_HOST=%s" % env.activate_vars['email_host'],
                     escape=False)

    # Setting EMAIL_PORT environment variable.
    print yellow('Checking for EMAIL_PORT...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export EMAIL_PORT=', exact=True):
        print yellow('Updating EMAIL_PORT...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export EMAIL_PORT=[.*]",
                  after="export EMAIL_PORT=%s" % env.activate_vars['email_port'],
                  backup='')
    else:
        print yellow('Adding EMAIL_PORT...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export EMAIL_PORT=%s" % env.activate_vars['email_port'],
                     escape=False)

    # Setting EMAIL_HOST_USER environment variable.
    print yellow('Checking for EMAIL_HOST_USER...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export EMAIL_HOST_USER=', exact=True):
        print yellow('Updating EMAIL_HOST_USER...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export EMAIL_HOST_USER=[.*]",
                  after="export EMAIL_HOST_USER=%s" % env.activate_vars['email_host_user'],
                  backup='')
    else:
        print yellow('Adding EMAIL_HOST_USER...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export EMAIL_HOST_USER=%s" % env.activate_vars['email_host_user'],
                     escape=False)

    # Setting EMAIL_HOST_PASSWORD environment variable.
    print yellow('Checking for EMAIL_HOST_PASSWORD...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export EMAIL_HOST_PASSWORD=', exact=True):
        print yellow('Updating EMAIL_HOST_PASSWORD...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export EMAIL_HOST_PASSWORD=[.*]",
                  after="export EMAIL_HOST_PASSWORD=%s" % env.activate_vars['email_host_password'],
                  backup='')
    else:
        print yellow('Adding EMAIL_HOST_PASSWORD...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export EMAIL_HOST_PASSWORD=%s" % env.activate_vars['email_host_password'],
                     escape=False)

    # Setting DB_HOST environment variable.
    print yellow('Checking for DB_HOST...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export DB_HOST=', exact=True):
        print yellow('Updating DB_HOST...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export DB_HOST=[.*]",
                  after="export DB_HOST=%s" % env.activate_vars['db_host'],
                  backup='')
    else:
        print yellow('Adding DB_HOST...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export DB_HOST=%s" % env.activate_vars['db_host'],
                     escape=False)

    # Setting DB_PORT environment variable.
    print yellow('Checking for DB_PORT...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export DB_PORT=', exact=True):
        print yellow('Updating DB_PORT...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export DB_PORT=[.*]",
                  after="export DB_PORT=%s" % env.activate_vars['db_port'],
                  backup='')
    else:
        print yellow('Adding DB_PORT...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export DB_PORT=%s" % env.activate_vars['db_port'],
                     escape=False)

    # Setting DB_NAME environment variable.
    print yellow('Checking for DB_NAME...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export DB_NAME=', exact=True):
        print yellow('Updating DB_NAME...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export DB_NAME=[.*]",
                  after="export DB_NAME=%s" % env.activate_vars['db_name'],
                  backup='')
    else:
        print yellow('Adding DB_NAME...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export DB_NAME=%s" % env.activate_vars['db_name'],
                     escape=False)

    # Setting DB_USER environment variable.
    print yellow('Checking for DB_USER...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export DB_USER=', exact=True):
        print yellow('Updating DB_USER...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export DB_USER=[.*]",
                  after="export DB_USER=%s" % env.activate_vars['db_user'],
                  backup='')
    else:
        print yellow('Adding DB_USER...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export DB_USER=%s" % env.activate_vars['db_user'],
                     escape=False)

    # Setting DB_PASSWORD environment variable.
    print yellow('Checking for DB_PASSWORD...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export DB_PASSWORD=', exact=True):
        print yellow('Updating DB_PASSWORD...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export DB_PASSWORD=[.*]",
                  after="export DB_PASSWORD=%s" % env.activate_vars['db_password'],
                  backup='')
    else:
        print yellow('Adding DB_PASSWORD...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export DB_PASSWORD=%s" % env.activate_vars['db_password'],
                     escape=False)

    # Setting DB_PASSWORD environment variable.
    print yellow('Checking for SECRET_KEY...')
    if files.contains('%(virtualenv_project_dir)s/bin/activate', text='export SECRET_KEY=', exact=True):
        print yellow('Updating SECRET_KEY...')
        files.sed("%(virtualenv_project_dir)s/bin/activate",
                  before="export SECRET_KEY=[.*]",
                  after="export SECRET_KEY=%s" % env.activate_vars['secret_key'],
                  backup='')
    else:
        print yellow('Adding SECRET_KEY...')
        files.append("%(virtualenv_project_dir)s/bin/activate" % env,
                     text="export SECRET_KEY=%s" % env.activate_vars['secret_key'],
                     escape=False)


@roles('web')
def bootstrap_web():
    if confirm(red('Are you sure you want to install the Web Server?'), default=False):
        print green('Bootstrapping Web Server')

        # Require some Debian/Ubuntu packages for the web server.
        print yellow('Installing python-dev, python-setuptools, git, nginx, libjpeg, zlib1g, libpq, memcached...')
        require.deb.packages([
            'python-dev',
            'python-setuptools',
            'git-core',
            'nginx',
            'libjpeg-dev',
            'zlib1g-dev',
            'libpq-dev',
            'memcached',
        ])

        # Install pip.
        if fabtools.python.is_pip_installed():
            print yellow('Python pip is already installed.')
        else:
            print yellow('Installing python pip...')
            fabtools.python.install_pip()

        # Install required supervisor python packages.
        print yellow('Installing supervisor python package...')
        fabtools.python.install(['supervisor==3.0b2', ], use_sudo=True)

        # Setup supervisor.
        if not files.exists('/etc/supervisord.conf'):
            with cd('/etc/'):
                print yellow('Creating /etc/supervisord.conf...')
                put('../configs/supervisor/supervisord.conf', 'supervisord.conf', use_sudo=True)

            if not files.exists(env.supervisord_dir):
                print yellow('Creating Supervisord directory...')
                print yellow('%(supervisord_dir)s' % env)
                sudo('mkdir %(supervisord_dir)s' % env)

                if not files.exists(env.supervisord_conf_dir):
                    print yellow('Creating Supervisord config directory...')
                    print yellow('%(supervisord_conf_dir)s' % env)
                    sudo('mkdir %(supervisord_conf_dir)s' % env)

        if not files.exists('/etc/init.d/supervisord'):
            with cd('/etc/init.d/'):
                print yellow('Sending supervisor running script...')
                put('../configs/supervisor/supervisord', 'supervisord', use_sudo=True)

            sudo('chmod +x /etc/init.d/supervisord')
            sudo('update-rc.d supervisord defaults')

            with settings(warn_only=True):
                sudo('service supervisord start')
        else:
            print yellow('Supervisor running script already exists.')

        # Setup virtualenv python package.
        with settings(warn_only=True):
            if not files.exists('%(virtualenv_dir)s' % env):
                print yellow('Creating virtual env directory virtualenv...')
                run('mkdir %(virtualenv_dir)s' % env)

        print yellow('Installing and setting up virtualenv...')
        require.python.virtualenv(env.virtualenv_project_dir)
    else:
        print red('User chose to skip Web Server installation.')


@roles('queue')
def bootstrap_rabbit():
    if confirm(red('Are you sure you want to install the RabbitMQ Server?'), default=False):
        print green('Bootstrapping RabbitMQ Server')

        # Add the rabbitmq repository.
        if not files.contains('/etc/apt/sources.list',
                              text='deb http://www.rabbitmq.com/debian/ testing main',
                              escape=False):
            print yellow('Adding RabbitMQ to sources.list...')
            files.append('/etc/apt/sources.list',
                         text='deb http://www.rabbitmq.com/debian/ testing main',
                         use_sudo=True)

        if not files.exists('/home/%(user)s/rabbitmq-signing-key-public.asc' % env):
            print yellow('Getting RabbitMQ signing key...')
            run('wget http://www.rabbitmq.com/rabbitmq-signing-key-public.asc')

            print yellow('Adding RabbitMQ signing key to apt...')
            fabtools.deb.add_apt_key('rabbitmq-signing-key-public.asc')

        print yellow('Installing python-dev, python-setuptools, git, rabbitmq-server, libjpeg, zlib1g, libpq, '
                     'memcached...')
        require.deb.packages([
            'python-dev',
            'python-setuptools',
            'git-core',
            'rabbitmq-server',
            'libjpeg-dev',
            'zlib1g-dev',
            'libpq-dev',
            'memcached',
        ])

        # Install the management plugin.
        print yellow('Installing RabbitMQ management plugins...')
        sudo('rabbitmq-plugins enable rabbitmq_management')

        # Install pip.
        if fabtools.python.is_pip_installed():
            print yellow('Python pip is already installed.')
        else:
            print yellow('Installing python pip...')
            fabtools.python.install_pip()

        # Setup virtualenv python package.
        with settings(warn_only=True):
            if not files.exists('%(virtualenv_dir)s' % env):
                print yellow('Creating virtual env directory virtualenv...')
                run('mkdir %(virtualenv_dir)s' % env)

        print yellow('Installing and setting up virtualenv...')
        require.python.virtualenv(env.virtualenv_project_dir)

        sudo('/etc/init.d/rabbitmq-server restart')

        # Install required supervisor python packages.
        print yellow('Installing supervisor python package...')
        fabtools.python.install(['supervisor==3.0b2', ], use_sudo=True)

        # Setup supervisor.
        if not files.exists('/etc/supervisord.conf'):
            with cd('/etc/'):
                print yellow('Creating /etc/supervisord.conf...')
                put('../configs/supervisor/supervisord.conf', 'supervisord.conf', use_sudo=True)

            if not files.exists(env.supervisord_dir):
                print yellow('Creating Supervisord directory...')
                print yellow('%(supervisord_dir)s' % env)
                sudo('mkdir %(supervisord_dir)s' % env)

                if not files.exists(env.supervisord_conf_dir):
                    print yellow('Creating Supervisord config directory...')
                    print yellow('%(supervisord_conf_dir)s' % env)
                    sudo('mkdir %(supervisord_conf_dir)s' % env)

        if not files.exists('/etc/init.d/supervisord'):
            with cd('/etc/init.d/'):
                print yellow('Sending supervisor running script...')
                put('../configs/supervisor/supervisord', 'supervisord', use_sudo=True)

            sudo('chmod +x /etc/init.d/supervisord')
            sudo('update-rc.d supervisord defaults')

            with settings(warn_only=True):
                sudo('service supervisord start')
        else:
            print yellow('Supervisor running script already exists.')
    else:
        print red('User chose to skip RabbitMQ Server installation.')


@roles('db')
def bootstrap_db():
    if confirm(red('Are you sure you want to install the Database Server?'), default=False):
        print green('Bootstrapping Database Server')

        # Install postgres.
        print yellow('Installing PostgreSQL 9.1...')
        require.postgres.server(version='9.1')
        print yellow('Creating PostgreSQL user...')
        require.postgres.user(env.activate_vars['db_user'], env.activate_vars['db_password'])
        print yellow('Creating PostgreSQL db...')
        require.postgres.database(env.activate_vars['db_name'], env.activate_vars['db_user'])

        # Edit pg_hba.conf to allow certain connections.
        with cd('/etc/postgresql/9.1/main/'):
            put('../configs/postgresql/pg_hba.conf', 'pg_hba.conf', mirror_local_mode=True, use_sudo=True)
            put('../configs/postgresql/postgresql.conf', 'postgresql.conf', mirror_local_mode=True, use_sudo=True)

            sudo('chown postgres:postgres pg_hba.conf')
            sudo('chown postgres:postgres postgresql.conf')

        files.sed('/etc/postgresql/9.1/main/pg_hba.conf',
                  before='<PG_HOST>',
                  after=env.activate_vars['db_host'],
                  backup='',
                  use_sudo=True)

        sudo('/etc/init.d/postgresql restart')
    else:
        print red('User chose to skip Web Server installation.')


def bootstrap():
    get_activate_vars()

    execute(bootstrap_web)
    execute(bootstrap_rabbit)
    execute(bootstrap_db)
    execute(write_env_vars)


def run_tests():
    print green('Running tests...')

    with settings(warn_only=True):
        if local('cd %(releases_dir)s' % env).failed:
            local('mkdir %(releases_dir)s' % env)

        if local('cd %(releases_project_dir)s' % env).failed:
            with lcd(env.releases_dir):
                local('git clone %(repo)s %(deployment_app_name)s' % env)
        else:
            with lcd(env.releases_project_dir):
                local('git pull origin master')

        with lcd('%(releases_project_dir)s/%(deployment_app_name)s' % env):
            result = local('workon %(project)s; python manage.py test --settings=%(project)s.settings.test' % env,
                           capture=False)

            if result.failed:
                abort(red('Aborting as the tests failed!') + yellow(' HEADS ARE GONNA ROLL!'))
            else:
                print green('All tests passed - ') + yellow(' HAVE A BANANA') + green('!')


@roles('web', 'queue')
def get_code():
    print green('Getting code...')

    if not files.exists(env.sites_dir):
        run('mkdir %(sites_dir)s' % env)

    with cd(env.sites_dir):
        if files.exists(env.sites_project_dir):
            with cd(env.deployment_app_name):
                run('git pull origin master' % env)
        else:
            # We need to check if we have a public key for BitBucket to use. If we do, then we can assume it has
            # already been added to the project on bitbucket. If not then we create one and print it to the user to
            # update the project before we clone.
            if not files.exists('/home/%(user)s/.ssh/id_rsa.pub' % env):
                print green('Looks like we need to create ssh keys for BitBucket to allow deployments...')
                run('ssh-keygen -q -t rsa -N "" -f /home/%(user)s/.ssh/id_rsa' % env)
                print
                run('cat /home/%(user)s/.ssh/id_rsa.pub' % env)
                print
                print yellow('Copy the key above and add the key here: '
                             'https://bitbucket.org/eurocom/%(project)s/admin/deploy-keys' % env)
                print
                prompt("Press ENTER once you've saved the key to continue...")

            run('git clone %(repo)s %(deployment_app_name)s' % env)

    # Create any missing directories that are needed.
    if not files.exists(env.sites_project_var_dir):
        print yellow('Creating project var directory...')
        print yellow('%(sites_project_var_dir)s' % env)
        run('mkdir %(sites_project_var_dir)s' % env)

    if not files.exists(env.sites_project_log_dir):
        print yellow('Creating project log directory...')
        print yellow('%(sites_project_log_dir)s' % env)
        run('mkdir %(sites_project_log_dir)s' % env)

    # Install python apps into the virtualenv.
    with fabtools.python.virtualenv(env.virtualenv_project_dir):
        print yellow('Installing apps from %(sites_project_dir)s/requirements/production.txt' % env)
        fabtools.python.install_requirements('%(sites_project_dir)s/requirements/production.txt' % env)


@roles('web')
def copy_web_configs():
    print green('Copying website configuration files...')

    with cd('%(sites_project_dir)s/configs/web/' % env):
        run('cp gunicorn_start.sh.sample gunicorn_start.sh')
        run('chmod +x gunicorn_start.sh')

    files.sed('%(sites_project_dir)s/configs/web/gunicorn_start.sh' % env,
              before='<APP>',
              after=env.project,
              backup='',
              use_sudo=True)
    files.sed('%(sites_project_dir)s/configs/web/gunicorn_start.sh' % env,
              before='<DEP_APP>',
              after=env.deployment_app_name,
              backup='',
              use_sudo=True)
    files.sed('%(sites_project_dir)s/configs/web/gunicorn_start.sh' % env,
              before='<USER>',
              after=env.user,
              backup='',
              use_sudo=True)


    with cd('%(supervisord_conf_dir)s' % env):
        put('../configs/supervisor/gunicorn.conf', '%(deployment_app_name)s_gunicorn.conf' % env, use_sudo=True)

    files.sed('%(supervisord_conf_dir)s/%(deployment_app_name)s_gunicorn.conf' % env,
              before='<APP>',
              after=env.project,
              backup='',
              use_sudo=True)
    files.sed('%(supervisord_conf_dir)s/%(deployment_app_name)s_gunicorn.conf' % env,
              before='<DEP_APP>',
              after=env.deployment_app_name,
              backup='',
              use_sudo=True)
    files.sed('%(supervisord_conf_dir)s/%(deployment_app_name)s_gunicorn.conf' % env,
              before='<USER>',
              after=env.user,
              backup='',
              use_sudo=True)

    with cd('/etc/nginx/sites-enabled/'):
        put('../configs/web/nginx.conf', '%(deployment_app_name)s.conlonyhq.com.conf' % env, use_sudo=True)

    files.sed('/etc/nginx/sites-enabled/%(deployment_app_name)s.conlonyhq.com.conf' % env,
              before='<APP>',
              after=env.project,
              backup='',
              use_sudo=True)
    files.sed('/etc/nginx/sites-enabled/%(deployment_app_name)s.conlonyhq.com.conf' % env,
              before='<DEP_APP>',
              after=env.deployment_app_name,
              backup='',
              use_sudo=True)
    files.sed('/etc/nginx/sites-enabled/%(deployment_app_name)s.conlonyhq.com.conf' % env,
              before='<USER>',
              after=env.user,
              backup='',
              use_sudo=True)


@roles('queue')
def copy_queue_configs():
    print green('Copying rabbitmq configuration files...')

    with cd('%(supervisord_conf_dir)s' % env):
        put('../configs/supervisor/rabbitmq.conf', '%(deployment_app_name)s_rabbitmq.conf' % env, use_sudo=True)

    files.sed('%(supervisord_conf_dir)s/%(deployment_app_name)s_rabbitmq.conf' % env,
              before='<APP>',
              after=env.project,
              backup='',
              use_sudo=True)
    files.sed('%(supervisord_conf_dir)s/%(deployment_app_name)s_rabbitmq.conf' % env,
              before='<DEP_APP>',
              after=env.deployment_app_name,
              backup='',
              use_sudo=True)
    files.sed('%(supervisord_conf_dir)s/%(deployment_app_name)s_rabbitmq.conf' % env,
              before='<USER>',
              after=env.user,
              backup='',
              use_sudo=True)


@roles('web')
def data_migration():
    print green('Doing data migrations...')

    with cd('%(sites_project_dir)s/%(project)s' % env):
        run('source %(virtualenv_project_dir)s/bin/activate; python manage.py syncdb --noinput '
            '--settings=%(project)s.settings.production' % env)
        run('source %(virtualenv_project_dir)s/bin/activate; python manage.py migrate '
            '--settings=%(project)s.settings.production' % env)
        run('source %(virtualenv_project_dir)s/bin/activate; python manage.py collectstatic --noinput '
            '--settings=%(project)s.settings.production' % env)


@roles('queue', 'web')
def restart_supervisor():
    print green('Restarting supervisor...')
    fabtools.supervisor.reload_config()
    fabtools.supervisor.restart_process(name='all')


@roles('web')
def restart_nginx():
    print green('Restarting nginx...')
    sudo('/etc/init.d/nginx stop')
    sudo('/etc/init.d/nginx start')


# def notify_newrelic():
#     try:
#         request = urllib2.Request(
#             url='https://rpm.newrelic.com/deployments.xml',
#             data=urllib.urlencode({
#                 'deployment[app_name]': env.deployment_app_name,
#                 'deployment[user]': socket.gethostname()
#             }),
#             headers={
#                 'X-api-key': '7ec3d5136e7b915b40512362ea6a9f1567ca2d678a3ad9f'
#             }
#         )
#         urllib2.urlopen(request)
#
#     except urllib2.HTTPError, e:
#         print 'Error reporting: ', e.code
#         print e.headers
#         print e.fp.read()


def deploy():
    execute(run_tests)
    execute(get_code)
    execute(copy_web_configs)
    execute(copy_queue_configs)
    execute(data_migration)
    execute(restart_supervisor)
    execute(restart_nginx)

    #notify_newrelic()
    done()
